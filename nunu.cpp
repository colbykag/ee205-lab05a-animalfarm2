///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <string>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu(bool newIsNative, enum Color newColor, enum Gender newGender ){
   
   isNative = newIsNative;

   gender = newGender;

   species = "Fistularia chinensis";

   scaleColor = newColor;

   favoriteTemp = 80.6;
}

///Print nunu stuff then fish stuff

void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << boolalpha;
   cout << "   Is native = [" << isNative << "]" << endl;
   Fish::printInfo();
}

}//namespace animalfarm
